# Practical Session 3 - Guides and Tips

## Changing the application name on Tomcat

As we are using IntelliJ to build and send our application to the Tomcat server, we need to change the _Running Configurations_ in order to set the base route this application. For example, when using `/pokemon` as the base route, all other routes will be affected like `/pokemon/pokemonTypes.html` and `/pokemon/api/pokemonTypes`. To change this, please set the field _Application context_ under the tab _Deployment_ to `/pokemon`.

![](./running-configurations.png)

## Checkout new branch

In order to retrieve the code for the exercises of the practical session, perform the step below on your git client. Here we'll be using IntelliJ's git client:

1. Find or list the remote list of your git client

    ![](./intellij-git-remote.png)

2. Fetch from the remote repository containing the original project all updates

    ![](./intellij-git-fetch-remote.png)

3. Checkout the branch `practical-session-3` and start working on it

    ![](./intellij-git-checkout-branch.png)

4. After committing your changes, push the branch to the upstream (i.e., to your fork on GitLab)

    ![](./intellij-git-push.png)

5. On GitLab, open a new Merge Request

Work on implementing the issues tagged as both `feature-request` and `practical-session-3`

## Running HTTP request on IntelliJ

With IntelliJ, you can create files ending with `.http` to write and run requests directly from the IDE. You can find sample files [here](./../design/requests) and the full documentation [here](https://www.jetbrains.com/help/idea/http-client-in-product-code-editor.html).

![](./http-client.animated.gif)


## REST API documentations with OpenAPI

OpenAPI is a standard for documenting REST APIs in JSON (or YAML). On IntelliJ, you can open the [our OpenAPI specification](./../design/open-api.yaml) and check all routes implemented or to be implemented, including examples.

![](./open-api.png)
